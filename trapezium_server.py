#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import http.server
import configparser
import os
import re
import shutil
import socket
import sys

class InstallServer(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        # Prepare response
        rep_ok = False
        action, arg = self.path.split('/')[1:3]
        if action == 'get':
            rep_ok = True
            if arg == 'default':
                rep = self.server.cluster.get_script()
            else:
                rep = self.server.cluster.get_profile(arg)
        elif action == 'remove':
            rep_ok = True
            self.server.cluster.remove_node(arg)
            rep = arg + ' removed'
        # Send response
        if rep_ok:
            self.send_response(200, 'OK')
            self.send_header('Content-type', 'text/plain')
            self.end_headers()
            self.wfile.write(bytes(rep, 'ascii'))
        else:
            self.send_response(404, 'Not found')
            self.end_headers()

class Cluster(object):
    def __init__(self, config, nodes):
        for k,v in config.items():
            setattr(self, k, v)
        self.nodes = nodes
        self.path_pxe_config = os.path.join(self.path_pxe, 'pxelinux.cfg')

    def _mac2node(self, mac):
        for k,v in self.nodes.items():
            if v['mac'] == mac:
                return k

    def init_pxe(self):
        if os.path.exists(self.path_pxe_config) is False:
            os.path.os.mkdir(self.path_pxe_config)
        if os.path.exists(os.path.join(self.path_pxe_config, 'default')) is False:
            shutil.copy(os.path.join(self.path_config, self.pxe_model_local_boot), os.path.join(self.path_pxe_config, 'default'))

    def update_pxe(self):
        for k,v in self.nodes.items():
            filepxe = os.path.join(self.path_pxe_config, '01-'+v['mac'].replace(':', '-'))
            if v['install']:
                pxe = open(os.path.join(self.path_config, self.pxe_model_install)).read()
                pxe = pxe.replace('REPLACE_ARCHISO_VERSION', self.archiso_version)
                pxe = pxe.replace('REPLACE_SCRIPT_URL', 'http://%s:%s/get/default'%(self.ip, self.port))
                with open(filepxe, 'w') as f:
                    f.write(pxe)
            else:
                if os.path.exists(filepxe):
                    os.remove(filepxe)

    def get_script(self):
        return open(os.path.join(self.path_config, self.install_script)).read()

    def get_profile(self, mac):
        node_model = open(os.path.join(self.path_config, self.install_template)).read()
        node = self._mac2node(mac.lower())
        if self.verbose:
            print('Install started on ' + node)
        profile = node_model.replace('REPLACE_HOSTNAME', node)
        return profile

    def remove_node(self, mac):
        node = self._mac2node(mac.lower())
        self.nodes[node]['install'] = False
        self.update_pxe()
        if self.verbose:
            print('Install finished on ' + node)

    def clean_nodes(self):
        print('Deleting PXE profile(s)')
        for f in os.listdir(self.path_pxe_config):
            if f.startswith('01-'):
                os.remove((os.path.join(self.path_pxe_config, f)))

def main(argv=None):
    # Parameters
    if argv is None:
        argv = sys.argv
    if len(argv) == 2:
        config_filename = argv[1]
    else:
        print('%s <config_filename>'%(argv[0]))
        return 2
    # ---- Config
    config = configparser.ConfigParser()
    config.read(config_filename)
    # Server config
    if not config.has_option('server', 'path_config'):
        config.set('server', 'path_config', os.path.dirname(os.path.abspath(config_filename)))
    if config.has_option('server', 'ip'):
        ip = config.getint('server', 'ip')
    else:
        ip = [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][0]
        config.set('server', 'ip', ip)
    if config.has_option('server', 'port'):
        port = config.getint('server', 'port')
    else:
        port = 8080
    verbose = config.getboolean('server', 'verbose')
    # Nodes config
    nodes = {}
    for s in config.sections():
        if s != 'server':
            nodes[s] = {'install': config.getboolean(s, 'install')}
            if config.has_option(s, 'mac'):
                nodes[s]['mac'] = config.get(s, 'mac').lower()
    if config.has_option('server', 'path_mac'):
        with open(config.get('server', 'path_mac')) as macfile:
            for l in macfile.readlines():
                if l[0] != '#':
                    fields = l.split(',')
                    mac = fields[0].lower()
                    hostname = fields[1]
                    if hostname in nodes:
                        nodes[hostname]['mac'] = mac
    # ---- Start cluster
    cluster = Cluster(dict(config.items('server')), nodes)
    cluster.init_pxe()
    cluster.update_pxe()
    # ---- Log
    if verbose:
        print('Starting server on %s:%s'%(ip, port))
        print('Nodes')
        for node in sorted(nodes.keys()):
            print('%-20s%-10s%s'%(node, nodes[node]['install'], nodes[node]['mac']))
    # ---- Start server
    server = http.server.HTTPServer((ip, port), InstallServer)
    server.cluster = cluster
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.cluster.clean_nodes()
        return 0

if __name__ == '__main__':
    sys.exit(main())
